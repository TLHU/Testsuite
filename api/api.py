""" this module interacts with Gitlab API"""
import os
import requests

BASE_URL = 'https://gitlab.com/api/v3'

def login():
    """Logs a user into Gitlab"""
    return requests.post(BASE_URL + "/session?login=" + os.environ['GITLAB_USER'] + "&password=" + os.environ['GITLAB_PW'])
    
def project_list():
    """Lists all projects for the logged in user"""
    headers = {'PRIVATE-TOKEN': os.environ['GITLAB_TOKEN']}
    return requests.get('https://gitlab.com/api/v3/projects', headers=headers)
  
