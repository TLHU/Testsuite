import sys
sys.path.insert(0, "api") 
"""0 means here relative folder from current dir"""

import api

def test_login():
    response = api.login()
    assert response.status_code >= 200 and response.status_code <= 299
    assert "tanczi@t-email.hu" in str(response.json())

def test_project_list():
    response = api.project_list()
    assert response.status_code >= 200 and response.status_code <= 299
